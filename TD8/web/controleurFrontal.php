<?php

use App\Covoiturage\Controleur\ControleurUtilisateur;
use App\Covoiturage\Lib\PreferenceControleur;

require_once __DIR__.'/../src/Lib/Psr4AutoloaderClass.php';

$chargeurDeClasse = new App\Covoiturage\Lib\Psr4AutoloaderClass(false);
$chargeurDeClasse->register();
$chargeurDeClasse->addNamespace('App\Covoiturage', __DIR__ . '/../src');

$action = $_GET["action"] ?? "afficherListe";

if (!isset($_GET["controleur"])) {
    $controleur = PreferenceControleur::lire() ?? "utilisateur";
}
else {
    $controleur = $_GET["controleur"];
}

$nomDeClasseControleur = "App\Covoiturage\Controleur\Controleur".ucfirst($controleur);
if(class_exists($nomDeClasseControleur))
{
    $functions = get_class_methods((new $nomDeClasseControleur));

    if (in_array($action, $functions))
    {
        (new $nomDeClasseControleur)::$action();
    }
    else
    {
        ControleurUtilisateur::afficherErreur("La fonction n'existe pas dans le controleur ".$nomDeClasseControleur);
    }
}
else
{
    (new ControleurUtilisateur)->afficherErreur("Controleur n'existe pas ou pas spécifié dans l'url");
}
