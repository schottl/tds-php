<?php

namespace App\Covoiturage\Lib;

use App\Covoiturage\Modele\HTTP\Session;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;

class ConnexionUtilisateur
{
    private static string $cleConnexion = "_utilisateurConnecte";

    public static function connecter(string $loginUtilisateur): void
    {
        Session::getInstance()->enregistrer(self::$cleConnexion, $loginUtilisateur);
    }

    public static function estConnecte(): bool
    {
        return Session::getInstance()->lire(self::$cleConnexion) !== null;
    }

    public static function deconnecter(): void
    {
        Session::getInstance()->detruire();
    }

    public static function getLoginUtilisateurConnecte(): ?string
    {
        return Session::getInstance()->lire(self::$cleConnexion) ?? null;
    }

    public static function estUtilisateur($login): bool{
        return self::getLoginUtilisateurConnecte() == $login;
    }

    public static function estAdmin(): bool{
        if (self::estConnecte()){
            $login = self::getLoginUtilisateurConnecte();
            $utilisateur = (new UtilisateurRepository)->recupererParClePrimaire($login);
            return $utilisateur->getEstAdmin();
        }
        return false;
    }

}