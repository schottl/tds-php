<?php
/** @var Trajet[] $trajets **/

use App\Covoiturage\Modele\DataObject\Trajet;

foreach ($trajets as $trajet){
    $idUrl = rawurlencode($trajet->getId());
    $idHTML = htmlspecialchars($trajet->getId());

    echo "<p>Trajet d'id  
            <a href='controleurFrontal.php?controleur=trajet&action=afficherDetails&id=".$idUrl."'>".$idHTML."</a>
        </p>";


    echo '<p>
            <a href="controleurFrontal.php?controleur=trajet&action=supprimer&id='.$idUrl.'">Supprimer</a>
            <a href="controleurFrontal.php?controleur=trajet&action=afficherFormulaireMiseAJour&id='.$idUrl.'">Modifier</a>
          </p>';
}

echo '<a href="controleurFrontal.php?controleur=trajet&action=afficherFormulaireCreation">Créer un trajet</a>';


