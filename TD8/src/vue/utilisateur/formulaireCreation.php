<form method="get" action="controleurFrontal.php">
<fieldset>
    <legend>Mon formulaire :</legend>
    <p class="InputAddOn">
        <label class="InputAddOn-item" for="login_id">Login</label>
        <input class="InputAddOn-field" type="text" placeholder="leblancj" name="login" id="login_id" required/>
    </p>
    <p class="InputAddOn">
        <label class="InputAddOn-item" for="nom">Nom</label>
        <input class="InputAddOn-field" type="text" placeholder="leblanc" name="nom" id="nom" required/>
    </p>
    <p class="InputAddOn">
        <label class="InputAddOn-item" for="prenom">Prenom</label>
        <input class="InputAddOn-field" type="text" placeholder="julien" name="prenom" id="prenom" required/>
    </p>
    <p class="InputAddOn">
        <label class="InputAddOn-item" for="mdp_id">Mot de passe&#42;</label>
        <input class="InputAddOn-field" type="password" value="" placeholder="" name="mdp" id="mdp_id" required>
    </p>
    <p class="InputAddOn">
        <label class="InputAddOn-item" for="mdp2_id">Vérification du mot de passe&#42;</label>
        <input class="InputAddOn-field" type="password" value="" placeholder="" name="mdp2" id="mdp2_id" required>
    </p>
    <p class="InputAddOn">
        <label class="InputAddOn-item" for="estAdmin_id">Administrateur</label>
        <input class="InputAddOn-field" type="checkbox" placeholder="" name="estAdmin" id="estAdmin_id">
    </p>
    <p class="InputAddOn">
        <input class="InputAddOn-field" type="submit" value="Envoyer" />
        <input type='hidden' name='action' value='creerDepuisFormulaire'>
    </p>
</fieldset>
</form>
