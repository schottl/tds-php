<?php

/** @var Utilisateur $utilisateur **/

use App\Covoiturage\Modele\DataObject\Utilisateur;

$login = htmlspecialchars($utilisateur->getLogin());
$nom = htmlspecialchars($utilisateur->getNom());
$prenom = htmlspecialchars($utilisateur->getPrenom());
$estAdmin = $utilisateur->getEstAdmin() ? 'Yes' : 'No';

echo "<p>Login: $login</p>";
echo "<p>Nom: $nom</p>";
echo "<p>Prénom: $prenom</p>";
echo "<p>Admin: $estAdmin</p>";