<form method="get" action="controleurFrontal.php">
    <fieldset>
        <legend>Connexion</legend>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="login_id">Login</label>
            <input class="InputAddOn-field" type="text" name="login" id="login_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="mdp_id">Mot de passe</label>
            <input class="InputAddOn-field" type="password" name="mdp" id="mdp_id" required>
        </p>
        <p class="InputAddOn">
            <input class="InputAddOn-field" type="submit" value="Se connecter">
            <input type='hidden' name='action' value='connecter'>

        </p>
    </fieldset>
</form>