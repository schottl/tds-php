<?php

use App\Covoiturage\Lib\ConnexionUtilisateur;
use App\Covoiturage\Modele\DataObject\Utilisateur;

/** @var Utilisateur $utilisateur */

$login = $utilisateur->getLogin();
$nom = $utilisateur->getNom();
$prenom = $utilisateur->getPrenom();



?>

<form method="get" action="controleurFrontal.php">
    <fieldset>
        <legend>Mon formulaire :</legend>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="login_id">Login</label>
                <input class="InputAddOn-field" type="text" value=<?= htmlspecialchars($login) ?> name="login" id="login_id" required readonly/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="nom">Nom</label>
            <input class="InputAddOn-field" type="text" value=<?=  htmlspecialchars($nom) ?> name="nom" id="nom" required/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="prenom">Prenom</label>
            <input class="InputAddOn-field" type="text" value=<?= htmlspecialchars($prenom) ?> name="prenom" id="prenom" required/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="old_mdp_id">Ancien mot de passe</label>
            <input class="InputAddOn-field" type="password" name="old_mdp" id="old_mdp_id">
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="new_mdp_id">Nouveau mot de passe*</label>
            <input class="InputAddOn-field" type="password" name="mdp" id="new_mdp_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="new_mdp2_id">Confirmer nouveau mot de passe*</label>
            <input class="InputAddOn-field" type="password" name="mdp2" id="new_mdp2_id" required>
        </p>
        <?php
            if (ConnexionUtilisateur::estAdmin()){
                echo'
                   <p class="InputAddOn">
                        <label class="InputAddOn-item" for="estAdmin_id">Administrateur</label>
                        <input class="InputAddOn-field" type="checkbox" placeholder="" name="estAdmin" id="estAdmin_id" ';
                if ($utilisateur->getEstAdmin()) echo 'checked';
                echo '</p>';
            }
        ?>

        <p class="InputAddOn">
            <input class="InputAddOn-field" type="submit" value="Mettre à jour" />
            <input type='hidden' name='action' value='mettreAJour'>
        </p>
    </fieldset>
</form>
