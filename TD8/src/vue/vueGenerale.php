<?php

/** @var string $titre **/
/** @var string $cheminCorpsVue **/

use App\Covoiturage\Lib\ConnexionUtilisateur;

?>

<!DOCTYPE html>
<html lang="fr">
    <meta charset="UTF-8">
    <title><?php echo $titre; ?></title>
    <link rel="stylesheet" href="../resources/css/style.css">
</head>
<body>
<header>
    <nav>
        <nav>
            <ul>
                <li>
                    <a href="controleurFrontal.php?action=afficherListe&controleur=utilisateur">Gestion des utilisateurs</a>
                </li>
                <li>
                    <a href="controleurFrontal.php?action=afficherListe&controleur=trajet">Gestion des trajets</a>
                </li>
                <li>
                    <a href="controleurFrontal.php?action=afficherFormulairePreference">
                        <img src="../resources/img/heart.png" >
                    </a>
                </li>

                <?php
                    if (!ConnexionUtilisateur::estConnecte()) {
                        echo '
                            <li>
                                <a href="controleurFrontal.php?action=afficherFormulaireCreation">
                                    <img src="../resources/img/add-user.png" >
                                </a>
                            </li>           

                            <li>
                                <a href="controleurFrontal.php?action=afficherFormulaireConnexion">
                                    <img src="../resources/img/enter.png" >
                                </a>
                            </li>';
                    }
                    else {
                        $login = ConnexionUtilisateur::getLoginUtilisateurConnecte();
                        echo '
                            <li>
                                <a href="controleurFrontal.php?action=afficherDetails&login=' . urlencode($login) . '">
                                    <img src="../resources/img/user.png" >
                                </a>
                            </li>';
                        echo '
                            <li>
                                <a href="controleurFrontal.php?action=deconnecter&login=' . urlencode($login) . '">
                                    <img src="../resources/img/logout.png" >
                                </a>
                            </li>';
                    }
                ?>
                </ul>
        </nav>
    </nav>
</header>
<main>
    <?php
    require __DIR__ . "/{$cheminCorpsVue}";
    ?>
</main>
<footer>
    <p>
        Site de covoiturage de Liam
    </p>
</footer>
</body>
</html>