<?php
namespace App\Covoiturage\Modele\DataObject;

use App\Covoiturage\Modele\Repository\UtilisateurRepository;
use Exception;


class Utilisateur extends AbstractDataObject
{

    private string $login;
    private string $nom;
    private string $prenom;
    private string $mdpHache;
    private bool $estAdmin;

    /**
     * @var Trajet[]|null
     */
    private ?array $trajetsCommePassager;

    public function getNom() : string {return $this->nom;}

    public function setNom(string $nom) {$this->nom = $nom;}

    public function getLogin() : string{return $this->login;}

    public function setLogin(string $login){$this->login = $login;}

    public function getPrenom() : string{return $this->prenom;}

    public function setPrenom(string $prenom){$this->prenom = $prenom;}

    public function getMdpHache(): string{return $this->mdpHache;}

    public function setMdpHache(string $mdpHache): void{$this->mdpHache = $mdpHache;}

    public function getEstAdmin(): bool{return $this->estAdmin;}

    public function setEstAdmin(bool $estAdmin): void{$this->estAdmin = $estAdmin;}

    /**
     * @throws Exception
     */
    public function getTrajetsCommePassager(Utilisateur $utilisateur): ?array
    {
        if($this->trajetsCommePassager == null)
        {
            $this->trajetsCommePassager = (new UtilisateurRepository)->recupererTrajetsCommePassager($utilisateur);
        }
        return $this->trajetsCommePassager;
    }

    public function setTrajetsCommePassager(?array $trajetsCommePassager): void{$this->trajetsCommePassager = $trajetsCommePassager;}

    // un constructeur
    public function __construct(
        string $login,
        string $nom,
        string $prenom,
        string $mdpHache,
        bool $estAdmin
    ){
        $this->login = $login;
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->trajetsCommePassager = null;
        $this->mdpHache = $mdpHache;
        $this->estAdmin = $estAdmin;
    }

    // Pour pouvoir convertir un objet en chaîne de caractères
    public function __toString() : string{
        return "Utilisateur " . $this->nom . " " . $this->prenom . " de login " . $this->login . " et passager des trajets " . $this->trajetsCommePassager;
    }





}
