<?php

namespace App\Covoiturage\Modele\HTTP;

class Cookie
{

    public static function enregistrer(string $cle, mixed $valeur, ?int $dureeExpiration = null): void
    {
        if (isset($dureeExpiration)) {
            $dureeExpiration = time() + $dureeExpiration;
        }
        else{
            $dureeExpiration = 0;
        }
        setcookie($cle, json_encode($valeur), $dureeExpiration);
    }

    public static function lire(string $cle): mixed{
        if(isset($_COOKIE[$cle]))
        {
            return json_decode($_COOKIE[$cle]);
        }
        return null;
    }

    public static function contient($cle) : bool
    {
        return isset($_COOKIE[$cle]);
    }

    public static function supprimer($cle) : void{
        if (self::contient($cle)) {
            setcookie($cle, "", 1);
            unset($_COOKIE[$cle]);
        }
    }









}