<?php

namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\Trajet;

use Exception;

class UtilisateurRepository extends AbstractRepository
{

    protected function getNomTable(): string
    {
        return 'utilisateur';
    }

    protected function getNomClePrimaire(): string
    {
        return 'login';
    }

    /** @return string[] */
    protected function getNomsColonnes(): array
    {
        return ["login", "nom", "prenom","mdpHache","estAdmin"];
    }

    protected function construireDepuisTableauSQL(array $utilisateurFormatTableau) : Utilisateur
    {
        return new Utilisateur(
            $utilisateurFormatTableau['login'],
            $utilisateurFormatTableau['nom'],
            $utilisateurFormatTableau['prenom'],
            $utilisateurFormatTableau['mdpHache'],
            $utilisateurFormatTableau['estAdmin']
        );
    }

    protected function formatTableauSQL(AbstractDataObject $utilisateur): array
    {
        /** @var Utilisateur $utilisateur */

        $estAdmin = 0;
        if ($utilisateur->getEstAdmin()) {
            $estAdmin = 1;
        }

        return array(
            "loginTag" => $utilisateur->getLogin(),
            "nomTag" => $utilisateur->getNom(),
            "prenomTag" => $utilisateur->getPrenom(),
            "mdpHacheTag" => $utilisateur->getMdpHache(),
            "estAdminTag" => $estAdmin
        );
    }

    /**
     * @return Trajet[]
     * @throws Exception
     */
    public static function recupererTrajetsCommePassager(Utilisateur $utilisateur) : array
    {
        $sql = "SELECT * FROM trajet
            JOIN passager ON trajet.id = passager.trajetId
            WHERE passager.login = :loginTag";

        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "loginTag" => $utilisateur->getLogin(),
        );
        $pdoStatement->execute($values);

        $trajets = $pdoStatement->fetchAll();
        $listeTrajets = [];

        foreach ($trajets as $trajetFormatTableau) {
            $newTrajet = (new TrajetRepository)->construireDepuisTableauSQL($trajetFormatTableau);
            $listeTrajets[] = $newTrajet;
        }

        return $listeTrajets;
    }


}