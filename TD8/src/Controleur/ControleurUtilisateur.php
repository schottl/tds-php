<?php

namespace App\Covoiturage\Controleur;

use App\Covoiturage\Lib\ConnexionUtilisateur;
use App\Covoiturage\Lib\MotDePasse;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\HTTP\Cookie;
use App\Covoiturage\Modele\HTTP\Session;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;

class ControleurUtilisateur extends ControleurGenerique
{

    public static function afficherListe() : void
    {
        $utilisateurs = (New UtilisateurRepository)->recuperer();
        self::afficherVue(
            'vueGenerale.php',
            [
                'utilisateurs' => $utilisateurs,
                "titre" => "Liste des utilisateurs",
                "cheminCorpsVue" => "utilisateur/liste.php"
            ],
        );
    }

    public static function afficherDetails() : void
    {
        if(!isset($_GET["login"])) {
            self::afficherErreur("login incorrect ou n'existe pas");
            return;
        }

        $login = $_GET["login"];
        $utilisateur = (New UtilisateurRepository)->recupererParClePrimaire($login);

        if ($utilisateur == null) {
            self::afficherErreur("l'utilisateur n'existe pas");
        }
        else {
            self::afficherVue(
                'vueGenerale.php',
                [
                    'utilisateur' => $utilisateur,
                    "titre" => "Details utilisateur",
                    "cheminCorpsVue" => "utilisateur/details.php"
                ]
            );
        }
    }

    public static function afficherFormulaireCreation() : void
    {
        self::afficherVue(
            'vueGenerale.php',
            [
                "titre" => "Formulaire de creation",
                "cheminCorpsVue" => "utilisateur/formulaireCreation.php"
            ]
        );
    }

    public static function creerDepuisFormulaire(): void
    {
        if (!isset($_GET["login"]) || !isset($_GET["nom"]) || !isset($_GET["prenom"]) || !isset($_GET["mdp"]) || !isset($_GET["mdp2"])){
            self::afficherErreur("Informations manquantes");
        }
        $utilisateur = self::construireDepuisFormulaire();
        var_dump($utilisateur);
        (New UtilisateurRepository)->ajouter($utilisateur);

        self::afficherVue(
            'vueGenerale.php',
            [
                "titre" => "Utilisateur Cree",
                "cheminCorpsVue" => "utilisateur/utilisateurCree.php",
                "utilisateurs" => (New UtilisateurRepository)->recuperer()
            ]
        );
    }

    public static function construireDepuisFormulaire(): Utilisateur
    {
        $login = $_GET["login"];
        $nom = $_GET["nom"];
        $prenom = $_GET["prenom"];
        $mdp = $_GET["mdp"];
        $mdp2 = $_GET["mdp2"];
        $estAdmin = isset($_GET["estAdmin"]);

        if ($mdp != $mdp2) {
            self::afficherErreur("Les mots de passe ne correspondent pas");
        }

        $mdpHache = MotDePasse::hacher($mdp);
        return new Utilisateur($login, $nom, $prenom,$mdpHache, $estAdmin);
    }

    public static function afficherErreur(string $messageErreur = ""): void
    {
        self::afficherVue(
            'vueGenerale.php',
            [
                "titre" => "Erreur",
                "cheminCorpsVue" => "utilisateur/erreur.php"
                ,"messageErreur" => $messageErreur
            ]
        );
    }

    public static function supprimer(): void
    {
        if (!isset($_GET["login"]))
        {
            self::afficherErreur("Le login doit etre entré en URL");
        }
        else
        {
            if ((new UtilisateurRepository)->recupererParClePrimaire($_GET["login"]) == null){
                self::afficherErreur("L'utilisateur n'existe pas");
                return;
            }

            if (!ConnexionUtilisateur::estUtilisateur($_GET["login"])){
                self::afficherErreur("Vous n'avez pas les droits pour supprimer cet utilisateur");
                return;
            }

            $login = $_GET["login"];
            (New UtilisateurRepository)->supprimer($login);
            self::afficherVue(
                'vueGenerale.php',
                [
                    "titre" => "Utilisateur supprimé",
                    "cheminCorpsVue" => "utilisateur/utilisateurSupprime.php",
                    "login" => $login,
                    "utilisateurs" => (New UtilisateurRepository)->recuperer()
                ]
            );
        }
    }

    public static function afficherFormulaireMiseAJour(): void
    {
        if (!isset($_GET["login"])){
            self::afficherErreur("login pas spécifié dans l'URL");
        }
        else
        {
            $login = $_GET["login"];

            if (!ConnexionUtilisateur::estUtilisateur($login)){
                self::afficherErreur("Vous n'avez pas les droits pour modifier cet utilisateur");
                return;
            }
            $utilisateur = (New UtilisateurRepository)->recupererParClePrimaire($login);

            self::afficherVue(
                'vueGenerale.php',
                [
                    "titre" => "Formulaire mise à jour",
                    "cheminCorpsVue" => "utilisateur/formulaireMiseAJour.php",
                    "utilisateur" => $utilisateur
                ]
            );
        }
    }

    public static function mettreAJour(): void
    {
        if (!isset($_GET["login"]) || !isset($_GET["nom"]) || !isset($_GET["prenom"]) || !isset($_GET["mdp"]) || !isset($_GET["mdp2"])){
            self::afficherErreur("Informations manquantes");
        }
        else
        {
            if ((new UtilisateurRepository)->recupererParClePrimaire($_GET["login"]) == null){
                self::afficherErreur("L'utilisateur n'existe pas");
                return;
            }

            if (!ConnexionUtilisateur::estUtilisateur($_GET["login"])){
                self::afficherErreur("Vous n'avez pas les droits pour modifier cet utilisateur");
                return;
            }

            $mdpHache = (new UtilisateurRepository)->recupererParClePrimaire($_GET["login"])->getMdpHache();
            if(!MotDePasse::verifier($_GET["mdp"],$mdpHache))
            {
                self::afficherErreur("L'ancien mot de passe est incorrect");
                return;
            }

            if ($_GET["mdp"] != $_GET["mdp2"]){
                self::afficherErreur("Les mots de passe ne correspondent pas");
                return;
            }

            $utilisateur = self::construireDepuisFormulaire();

            (New UtilisateurRepository)->mettreAJour($utilisateur);

            self::afficherVue(
                'vueGenerale.php',
                [
                    "titre" => "Mise à jour",
                    "cheminCorpsVue" => "utilisateur/utilisateurMisAJour.php",
                    "login" => $utilisateur->getLogin(),
                    "utilisateurs" => (New UtilisateurRepository)->recuperer()
                ]
            );
        }
    }

    public static function afficherFormulaireConnexion(): void
    {
        self::afficherVue(
            'vueGenerale.php',
            [
                "titre" => "Formulaire connexion",
                "cheminCorpsVue" => "utilisateur/formulaireConnexion.php",
            ]
        );
    }

    public static function connecter() : void
    {
        if (!isset($_GET['login']) || !isset($_GET['mdp'])){
            self::afficherErreur('Login et/ou mdp manquant');
        }

        $login=$_GET['login'];
        $mdp=$_GET['mdp'];

        $utilisateur = (new UtilisateurRepository)->recupererParClePrimaire($login);
        if ($utilisateur){
            $mdpHache = $utilisateur->getMdpHache();
            if (MotDePasse::verifier($mdp,$mdpHache)){
                ConnexionUtilisateur::connecter($login);
                self::afficherVue(
                    'vueGenerale.php',
                    [
                        "titre" => "Utilisateur connecté",
                        "cheminCorpsVue" => "utilisateur/utilisateurConnecte.php",
                        "utilisateur" => $utilisateur
                    ]
                );
            }
            else{
                self::afficherErreur('Mot de passe incorrect');
            }
        }
        else{
            self::afficherErreur('Login incorrect');
        }
    }

    public static function deconnecter() : void
    {
        ConnexionUtilisateur::deconnecter();
        self::afficherVue(
            'vueGenerale.php',
            [
                "titre" => "Deconnexion",
                "cheminCorpsVue" => "utilisateur/deconnexion.php",
                "utilisateurs" => (New UtilisateurRepository)->recuperer()
            ]
        );
    }

}
