<?php

namespace App\Covoiturage\Controleur;

use App\Covoiturage\Modele\DataObject\Trajet;
use App\Covoiturage\Modele\Repository\TrajetRepository;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;
use DateTime;
use Exception;
use http\Exception\BadMessageException;

class ControleurTrajet extends ControleurGenerique
{
    /**
     * @throws Exception
     */
    public static function afficherListe() : void
    {
        $trajets = (new TrajetRepository)->recuperer();
        self::afficherVue(
            'vueGenerale.php',
            [
                'trajets' => $trajets,
                "titre" => "Liste des trajets",
                "cheminCorpsVue" => "trajet/liste.php"
            ],
        );
    }

    public static function afficherErreur(string $messageErreur = ""){
        self::afficherVue(
            'vueGenerale.php',
            [
                "titre" => "Erreur",
                "cheminCorpsVue" => "trajet/erreur.php"
                ,"messageErreur" => $messageErreur
            ]
        );
    }

    public static function afficherDetails() : void
    {
        if(!isset($_GET["id"])) {
            self::afficherErreur("ID incorrect ou n'existe pas");
            return;
        }

        $id = $_GET["id"];
        $trajet = (New TrajetRepository())->recupererParClePrimaire($id);

        if ($trajet == null) {
            self::afficherErreur("le trajet n'existe pas");
        }
        else {
            self::afficherVue(
                'vueGenerale.php',
                [
                    'trajet' => $trajet,
                    "titre" => "Details trajet",
                    "cheminCorpsVue" => "trajet/details.php"
                ]
            );
        }
    }

    public static function supprimer()
    {
        if (!isset($_GET["id"]))
        {
            self::afficherErreur("L'id doit etre entré en URL");
        }
        else
        {
            $id = $_GET["id"];
            (New TrajetRepository())->supprimer($id);
            self::afficherVue(
                'vueGenerale.php',
                [
                    "titre" => "Trajet supprimé",
                    "cheminCorpsVue" => "trajet/trajetSupprime.php",
                    "id" => $id,
                    "trajets" => (New TrajetRepository)->recuperer()
                ]
            );
        }
    }

    public static function afficherFormulaireCreation(){
        self::afficherVue(
            'vueGenerale.php',
            [
                "titre" => "Formulaire création trajet",
                "cheminCorpsVue" => "trajet/formulaireCreation.php"
            ]
        );
    }

    /**
     * @throws Exception
     */
    public static function creerDepuisFormulaire(): void
    {
        if (!self::isSetFormulaire($_GET))
        {
            self::afficherErreur("Valeur de formulaire manquante");
            return;
        }
        $trajet = self::construireDepuisFormulaire($_GET);
        (New TrajetRepository)->ajouter($trajet);

        self::afficherVue(
            'vueGenerale.php',
            [
                "titre" => "Trajet Cree",
                "cheminCorpsVue" => "trajet/trajetCree.php",
                "trajets" => (New TrajetRepository)->recuperer()
            ]
        );
    }


    public static function afficherFormulaireMiseAJour()
    {
        if (!isset($_GET["id"])){
            self::afficherErreur("id pas spécifié dans l'URL");
        }
        else
        {
            $login = $_GET["id"];
            $trajet = (New TrajetRepository)->recupererParClePrimaire($login);

            self::afficherVue(
                'vueGenerale.php',
                [   
                    "titre" => "Formulaire mise à jour",
                    "cheminCorpsVue" => "trajet/formulaireMiseAJour.php",
                    "trajet" => $trajet
                ]
            );
        }
    }

    /**
     * @throws Exception
     */
    public static function mettreAJour()
    {
        if (!self::isSetFormulaire($_GET))
        {
            self::afficherErreur("valeurs manquantes");
        }
        else
        {
           $trajet = self::construireDepuisFormulaire($_GET);
           (New TrajetRepository)->mettreAJour($trajet);

            self::afficherVue(
                'vueGenerale.php',
                [
                    "titre" => "Mise à jour",
                    "cheminCorpsVue" => "trajet/trajetMisAJour.php",
                    "id" => $_GET['id'],
                    "trajets" => (New TrajetRepository)->recuperer()
                ]
            );
        }

    }

    /**
     * @return Trajet
     * @throws Exception
     */
    public static function construireDepuisFormulaire($tableauDonneesFormulaire): Trajet
    {
        $id = $tableauDonneesFormulaire["id"] ?? null;
        $depart = $tableauDonneesFormulaire["depart"];
        $arrivee = $tableauDonneesFormulaire["arrivee"];
        $date = new DateTime($tableauDonneesFormulaire["date"]);
        $prix = $tableauDonneesFormulaire["prix"];
        $conducteur = (new UtilisateurRepository)->recupererParClePrimaire($tableauDonneesFormulaire["conducteurLogin"]);
        $nonFumeur = isset($tableauDonneesFormulaire["nonFumeur"]);

        $trajet = new Trajet($id, $depart, $arrivee, $date, $prix, $conducteur, $nonFumeur);
        return $trajet;
    }

    /**
     * @return bool
     */
    public static function isSetFormulaire($tableauDonneesFormulaire): bool
    {
        return
            isset($tableauDonneesFormulaire["depart"]) &&
            isset($tableauDonneesFormulaire["arrivee"]) &&
            isset($tableauDonneesFormulaire["date"]) &&
            isset($tableauDonneesFormulaire["prix"]) &&
            isset($tableauDonneesFormulaire["conducteurLogin"]);
    }

}
