<?php

namespace App\Covoiturage\Configuration;

class ConfigurationSite
{
    static private array $configurationSite = array(
        'dureeExpiration' => 3600,
    );

    public static function getDureeExpiration() : string {
        return ConfigurationSite::$configurationSite['dureeExpiration'];
    }

}