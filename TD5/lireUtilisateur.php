<?php

use App\Covoiturage\Modele\ModeleUtilisateur;

$utilisateurs = ModeleUtilisateur::recupererUtilisateurs();

foreach ($utilisateurs as $utilisateur) {
    echo $utilisateur . "\n";
}