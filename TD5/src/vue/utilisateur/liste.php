<?php
/** @var ModeleUtilisateur[] $utilisateurs **/

use App\Covoiturage\Modele\ModeleUtilisateur;

foreach ($utilisateurs as $utilisateur){
    $loginUrl = rawurlencode($utilisateur->getLogin());
    $loginHTML = htmlspecialchars($utilisateur->getLogin());

    echo '<p> Utilisateur de login
        <a href="controleurFrontal.php?action=afficherDetails&login='.$loginUrl.'">'.$loginHTML.'</a></p>';
}

echo '<a href="controleurFrontal.php?action=afficherFormulaireCreation">Créer un utilisateur</a>';

