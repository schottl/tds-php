<?php

/** @var string $titre **/
/** @var string $cheminCorpsVue **/
?>

<!DOCTYPE html>
<html lang="fr">
    <meta charset="UTF-8">
    <title><?php echo $titre; ?></title>
    <link rel="stylesheet" href="../resources/css/style.css">
</head>
<body>
<header>
    <nav>
        <nav>
            <ul>
                <li>
                    <a href="controleurFrontal.php?action=afficherListe&controleur=utilisateur">Gestion des utilisateurs</a>
                </li><li>
                    <a href="controleurFrontal.php?action=afficherListe&controleur=trajet">Gestion des trajets</a>
                </li>
            </ul>
        </nav>
    </nav>
</header>
<main>
    <?php
    require __DIR__ . "/{$cheminCorpsVue}";
    ?>
</main>
<footer>
    <p>
        Site de covoiturage de Liam
    </p>
</footer>
</body>
</html>