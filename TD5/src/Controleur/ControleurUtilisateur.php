<?php

namespace App\Covoiturage\Controleur;

use App\Covoiturage\Modele\ModeleUtilisateur;

class ControleurUtilisateur
{

    private static function afficherVue(
        string $cheminVue,
        array $parametres = []) : void
    {
        extract($parametres);
        require __DIR__."/../vue/$cheminVue";

    }

    public static function afficherListe() : void
    {
        $utilisateurs = ModeleUtilisateur::recupererUtilisateurs();
        self::afficherVue(
            'vueGenerale.php',
            [
                'utilisateurs' => $utilisateurs,
                "titre" => "Liste des utilisateurs",
                "cheminCorpsVue" => "utilisateur/liste.php"
            ],
        );
    }

    public static function afficherDetails() : void
    {
        if(!isset($_GET["login"])) {
            self::afficherVue(
                'vueGenerale.php',
                [
                    "titre" => "Erreur",
                    "cheminCorpsVue" => "utilisateur/erreur.php"
                ]
            );
            return;
        }

        $login = $_GET["login"];
        $utilisateur = ModeleUtilisateur::getUtilisateurParLogin($login);

        if ($utilisateur == null) {
            self::afficherVue(
                'vueGenerale.php',
                [
                    "titre" => "Erreur",
                    "cheminCorpsVue" => "utilisateur/erreur.php"
                ]
            );
        }
        else {
            self::afficherVue(
                'vueGenerale.php',
                [
                    'utilisateur' => $utilisateur,
                    "titre" => "Details utilisateur",
                    "cheminCorpsVue" => "utilisateur/details.php"
                ]
            );
        }
    }

    public static function afficherFormulaireCreation() : void
    {
        self::afficherVue(
            'vueGenerale.php',
            [
                "titre" => "Formulaire de creation",
                "cheminCorpsVue" => "utilisateur/formulaireCreation.php"
            ]
        );
    }

    public static function creerDepuisFormulaire(): void
    {
        if (!isset($_GET["login"]) || !isset($_GET["nom"]) || !isset($_GET["prenom"])) {
            self::afficherVue(
                'vueGenerale.php',
                [
                    "titre" => "Erreur",
                    "cheminCorpsVue" => "utilisateur/erreur.php"
                ]
            );
            return;
        }
        $login = $_GET["login"];
        $nom = $_GET["nom"];
        $prenom = $_GET["prenom"];

        $utilisateur = new ModeleUtilisateur($login, $nom, $prenom);
        $utilisateur->ajouter();

        self::afficherVue(
            'vueGenerale.php',
            [
                "titre" => "Utilisateur Cree",
                "cheminCorpsVue" => "utilisateur/utilisateurCree.php",
                "utilisateurs" => ModeleUtilisateur::recupererUtilisateurs()
            ]
        );
    }



}
