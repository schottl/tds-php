
        <div>
            <form method="get" action="formulaireLectureUtilisateur.php">
                <fieldset>
                    <legend>Retrouve un utilisateur par son login :</legend>
                    <p>
                        <label for="login_id">Login</label> :
                        <input type="text" placeholder="Ex : leblancj" name="login" id="login_id" required/>
                    </p>
                    <p>
                        <input type="submit" value="Envoyer" />
                    </p>
                </fieldset>
            </form>
        </div>
        <?php

        use App\Covoiturage\Modele\ConnexionBaseDeDonnees;
        use App\Covoiturage\Modele\ModeleUtilisateur;

        require_once __DIR__.'/../ConnexionBaseDeDonnees.php';
        require_once 'ModeleUtilisateur.php';

        function recupererUtilisateurParLogin(string $login) : ?ModeleUtilisateur {
            $sql = "SELECT * from utilisateur2 WHERE login='$login'";
            echo "<p>J'effectue la requête <pre>$sql</pre></p>";
            $pdoStatement = ConnexionBaseDeDonnees::getPdo()->query($sql);
            $utilisateurTableau = $pdoStatement->fetch();
            if ($utilisateurTableau !== false) {
                return ModeleUtilisateur::construireDepuisTableauSQL($utilisateurTableau);
            }
            return null;
        }

        if (isset($_GET['login'])) {
            $u = recupererUtilisateurParLogin($_GET['login']);
            echo $u;
        }
        ?>