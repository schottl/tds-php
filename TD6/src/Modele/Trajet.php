<?php

namespace App\Covoiturage\Modele;

use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\Repository\ConnexionBaseDeDonnees;
use DateTime;
use Exception;
use PDOException;

class Trajet {

    private ?int $id;
    private string $depart;
    private string $arrivee;
    private DateTime $date;
    private int $prix;
    private Utilisateur $conducteur;
    private bool $nonFumeur;
    /**
     * @var Utilisateur[]
     */
    private array $passagers;


    public function __construct(?int $id, string $depart, string $arrivee, DateTime $date, int $prix, Utilisateur $conducteur, bool $nonFumeur)
    {
        $this->id = $id;
        $this->depart = $depart;
        $this->arrivee = $arrivee;
        $this->date = $date;
        $this->prix = $prix;
        $this->conducteur = $conducteur;
        $this->nonFumeur = $nonFumeur;
        $this->passagers = [];
    }

    /**
     * @throws Exception
     */
    public static function construireDepuisTableauSQL(array $trajetTableau) : Trajet {
        $trajet = new Trajet(
            $trajetTableau["id"],
            $trajetTableau["depart"],
            $trajetTableau["arrivee"],
            new DateTime($trajetTableau["date"]), // À changer
            $trajetTableau["prix"],
            Utilisateur::getUtilisateurParLogin($trajetTableau["conducteurLogin"]), // À changer
            $trajetTableau["nonFumeur"], // À changer ?
            []
        );
        $trajet->recupererPassagers();

        return $trajet;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getDepart(): string
    {
        return $this->depart;
    }

    public function setDepart(string $depart): void
    {
        $this->depart = $depart;
    }

    public function getArrivee(): string
    {
        return $this->arrivee;
    }

    public function setArrivee(string $arrivee): void
    {
        $this->arrivee = $arrivee;
    }

    public function getDate(): DateTime
    {
        return $this->date;
    }

    public function setDate(DateTime $date): void
    {
        $this->date = $date;
    }

    public function getPrix(): int
    {
        return $this->prix;
    }

    public function setPrix(int $prix): void
    {
        $this->prix = $prix;
    }

    public function getConducteur(): Utilisateur
    {
        return $this->conducteur;
    }

    public function setConducteur(Utilisateur $conducteur): void
    {
        $this->conducteur = $conducteur;
    }

    public function isNonFumeur(): bool
    {
        return $this->nonFumeur;
    }

    public function setNonFumeur(bool $nonFumeur): void
    {
        $this->nonFumeur = $nonFumeur;
    }

    public function getPassagers(): array
    {
        return $this->passagers;
    }

    public function setPassagers(array $passagers): void
    {
        $this->passagers = $passagers;
    }



    public function __toString()
    {
        $nonFumeur = $this->nonFumeur ? " non fumeur" : " ";
        return "<p>
            Le trajet$nonFumeur du {$this->date->format("d/m/Y")} 
            partira de {$this->depart} pour aller à {$this->arrivee}
            (conducteur: {$this->conducteur->getPrenom()} {$this->conducteur->getNom()}).
        </p>";
    }

    /**
     * @return Trajet[]
     * @throws Exception
     */
    public static function getTrajets() : array {
        $pdoStatement = ConnexionBaseDeDonnees::getPDO()->query("SELECT * FROM trajet");

        $trajets = [];
        foreach($pdoStatement as $trajetFormatTableau) {
            $trajets[] = Trajet::construireDepuisTableauSQL($trajetFormatTableau);
        }

        return $trajets;
    }

    public function ajouter() : bool {
        $sql = "INSERT INTO trajet (id, depart, arrivee, date, prix, conducteurLogin, nonFumeur) 
                VALUES (:idTag, :departTag, :arriveeTag, :dateTag, :prixTag, :conducteurLoginTag, :nonFumeurTag)";

        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $nonFumeurInt = 0;
        if($this->nonFumeur){
            $nonFumeurInt = 1;
        }

        $values = array(
            "idTag" => $this->id,
            "departTag" => $this->depart,
            "arriveeTag" => $this->arrivee,
            "dateTag" => $this->date->format("Y-m-d"),
            "prixTag" => $this->prix,
            "conducteurLoginTag" => $this->conducteur->getLogin(),
            "nonFumeurTag" => $nonFumeurInt
        );
        try {
            $pdoStatement->execute($values);
            $newId = ConnexionBaseDeDonnees::getPdo()->lastInsertId();
            echo $newId;
            return true;
        } catch(PDOException $e) {
            return false;
        }
    }


    /**
     * @return Utilisateur[]
     */
    private function recupererPassagers() : ?array {
        $sql = "SELECT DISTINCT passagerlogin FROM passager  
                INNER JOIN trajet ON trajet.id = passager.trajetId
                WHERE trajet.id = :id";

        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "id" => $this->id,
        );
        $pdoStatement->execute($values);

        $utilisateursLogins = $pdoStatement->fetch();

        foreach ($utilisateursLogins as $utilisateur) {
            $this->setPassagers([Utilisateur::getUtilisateurParLogin($utilisateur)]);
        }
        return $this->passagers;

    }

    public function supprimerPassager(string $passagerLogin): ?bool{
        $sql = "DELETE from passager WHERE trajetId = :idTag and passagerLogin = :passagerLogin";
        // Préparation de la requête
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "idTag" => $this->id,
            'passagerLogin' => $passagerLogin,
        );
        $pdoStatement->execute($values);
        if($pdoStatement->rowCount() == 1) {
            return true;
        }
        return false;
    }

    /**
     * @throws Exception
     */
    public static function recupererTrajetParId($id): ?Trajet
    {
        $sql = "SELECT * from trajet WHERE id = :idTag";
        // Préparation de la requête
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "idTag" => $id,
        );
        $pdoStatement->execute($values);

        $trajetFormatTableau = $pdoStatement->fetch();
        if($trajetFormatTableau === false){
            return null;
        }
        return Trajet::construireDepuisTableauSQL($trajetFormatTableau);

    }

}
