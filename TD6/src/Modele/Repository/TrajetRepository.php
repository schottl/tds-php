<?php

namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\DataObject\Trajet;
use DateTime;
use Exception;

class TrajetRepository extends  AbstractRepository
{
    protected function getNomsColonnes(): array
    {
        return ["id", "depart", "arrivee", "date","prix","conducteurLogin","nonFumeur"];
    }

    protected function formatTableauSQL(AbstractDataObject $trajet): array
    {
        /** @var Trajet $trajet */
        return array(
            "idTag" => $trajet->getId(),
            "departTag" => $trajet->getDepart(),
            "arriveeTag" => $trajet->getArrivee(),
            "dateTag" => $trajet->getDate()->format("Y-m-d"),
            "prixTag" => $trajet->getPrix(),
            "conducteurLoginTag" => $trajet->getConducteur()->getLogin(),
            "nonFumeurTag" => $trajet->isNonFumeur() ? 1 : 0,
        );
    }

    public function getNomTable(): string
    {
        return 'trajet';
    }

    protected function getNomClePrimaire(): string
    {
        return 'id';
    }

    /**
     * @throws Exception
     */
    protected function construireDepuisTableauSQL(array $trajetTableau) : Trajet {
        $trajet = new Trajet(
            $trajetTableau["id"],
            $trajetTableau["depart"],
            $trajetTableau["arrivee"],
            new DateTime($trajetTableau["date"]),
            $trajetTableau["prix"],
            (new UtilisateurRepository)->recupererParClePrimaire($trajetTableau["conducteurLogin"]), // À changer
            $trajetTableau["nonFumeur"],
            []
        );
        (new TrajetRepository)->recupererPassagers($trajet);

        return $trajet;
    }

    /**
     * @return Utilisateur[]
     */
    static public function recupererPassagers(Trajet $trajet) : ?array {
        $sql = "SELECT DISTINCT passagerlogin FROM passager  
                INNER JOIN trajet ON trajet.id = passager.trajetId
                WHERE trajet.id = :id";

        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "id" => $trajet->getId(),
        );
        $pdoStatement->execute($values);

        $utilisateursLogins = $pdoStatement->fetch();

        if ($utilisateursLogins!=[]){
            foreach ($utilisateursLogins as $utilisateur) {
                $trajet->setPassagers([(new UtilisateurRepository)->recupererParClePrimaire($utilisateur)]);
            }
        }
        return $trajet->getPassagers();

    }

}