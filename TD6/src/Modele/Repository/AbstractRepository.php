<?php

namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\DataObject\AbstractDataObject;

abstract class AbstractRepository
{

    public function ajouter(AbstractDataObject $objet): bool
    {

        $colonnes = $this->getNomsColonnes();
        $tags = [];
        foreach ($colonnes as $nomColonne) {
            $tags[] = ":".$nomColonne."Tag";
        }

        $sql = "INSERT INTO ".$this->getNomTable()." (".implode(',',$this->getNomsColonnes()).
                ") VALUES (".implode(', ',$tags).")";


        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = $this->formatTableauSQL($objet);
        $pdoStatement->execute($values);
        return true;
    }

    public function supprimer($valeurClePrimaire)
    {
        $sql = "DELETE FROM ".$this->getNomTable()." WHERE ".$this->getNomClePrimaire()." = :".$this->getNomClePrimaire()."Tag";

        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            $this->getNomClePrimaire()."Tag" => $valeurClePrimaire,
        );

        $pdoStatement->execute($values);
    }

    protected abstract function getNomTable(): string;

    protected abstract function getNomClePrimaire(): string;

    /** @return string[] */
    protected abstract function getNomsColonnes(): array;

    protected abstract function construireDepuisTableauSQL(array $objetFormatTableau) : AbstractDataObject;

    protected abstract function formatTableauSQL(AbstractDataObject $objet): array;

    public function recuperer() : array
    {
        $nomtable = $this->getNomTable();
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->query("SELECT * FROM ".$nomtable);

        $utilisateurs = array();
        foreach($pdoStatement as $utilisateurFormatTableau){
            $utilisateurs[] = $this->construireDepuisTableauSQL($utilisateurFormatTableau);
        }
        return $utilisateurs;
    }

    public function recupererParClePrimaire(string $clePrimaire): ?AbstractDataObject
    {
        $sql = "SELECT * from ".$this->getNomTable()." WHERE ".$this->getNomClePrimaire()." = :".$this->getNomClePrimaire()."Tag";

        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            $this->getNomClePrimaire()."Tag" => $clePrimaire,
        );
        $pdoStatement->execute($values);

        $objetFormatTableau = $pdoStatement->fetch();
        if ($objetFormatTableau === false) {
            return null;
        }
        return $this->construireDepuisTableauSQL($objetFormatTableau);
    }

    public function mettreAJour(AbstractDataObject $objet): void
    {
        $colonnes = $this->getNomsColonnes();

        $tags = [];
        foreach ($colonnes as $colonne) {
            $tags[] = $colonne . "= :" . $colonne . "Tag";
        }
        $str = implode(", ", $tags);

        $sql = "UPDATE " . $this->getNomTable() . " SET " . $str
            . " WHERE " . $this->getNomClePrimaire() . " = :" . $this->getNomClePrimaire() . "Tag";

        var_dump($sql);
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $values = $this->formatTableauSQL($objet);

        $pdoStatement->execute($values);
    }


}