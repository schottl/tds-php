<?php
use App\Covoiturage\Modele\DataObject\Utilisateur;

/** @var Utilisateur $utilisateur */

$login = $utilisateur->getLogin();
$nom = $utilisateur->getNom();
$prenom = $utilisateur->getPrenom();



?>

<form method="get" action="controleurFrontal.php">
    <fieldset>
        <legend>Mon formulaire :</legend>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="login_id">Login</label>
                <input class="InputAddOn-field" type="text" value=<?= htmlspecialchars($login) ?> name="login" id="login_id" required readonly/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="nom">Nom</label>
            <input class="InputAddOn-field" type="text" value=<?=  htmlspecialchars($nom) ?> name="nom" id="nom" required/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="prenom">Prenom</label>
            <input class="InputAddOn-field" type="text" value=<?= htmlspecialchars($prenom) ?> name="prenom" id="prenom" required/>
        </p>
        <p class="InputAddOn">
            <input class="InputAddOn-field" type="submit" value="Envoyer" />
            <input type='hidden' name='action' value='mettreAJour'>
        </p>
    </fieldset>
</form>
