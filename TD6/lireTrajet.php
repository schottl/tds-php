<?php

use App\Covoiturage\Modele\Trajet;

try {
    $trajets = Trajet::getTrajets();
} catch (Exception $e) {
}

foreach ($trajets as $trajet) {
    echo 'Trajet : '.$trajet->getId()."\n";
    echo "Passagers :\n";
    $passagers = $trajet->getPassagers();
    foreach ($passagers as $passager) {
        echo 'Passager : '.$passager->getLogin()."\n";
    }

}