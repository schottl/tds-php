<?php

use App\Covoiturage\Controleur\ControleurUtilisateur;
require_once __DIR__.'/../src/Lib/Psr4AutoloaderClass.php';

// initialisation en activant l'affichage de débogage
$chargeurDeClasse = new App\Covoiturage\Lib\Psr4AutoloaderClass(false);
$chargeurDeClasse->register();
// enregistrement d'une association "espace de nom" → "dossier"
$chargeurDeClasse->addNamespace('App\Covoiturage', __DIR__ . '/../src');



// On récupère l'action passée dans l'URL
$action = $_GET["action"] ?? "afficherListe";

$controleur = $_GET['controleur'] ?? 'utilisateur';
$nomDeClasseControleur = "App\Covoiturage\Controleur\Controleur".ucfirst($controleur);

if(class_exists($nomDeClasseControleur))
{
    $functions = get_class_methods((new $nomDeClasseControleur));

    if (in_array($action, $functions))
    {
        (new $nomDeClasseControleur)::$action();
    }
    else
    {
        ControleurUtilisateur::afficherErreur("La fonction n'existe pas dans le controleur ".$nomDeClasseControleur);
    }
}
else
{
    (new ControleurUtilisateur)->afficherErreur("Controleur n'existe pas ou pas spécifié dans l'url");
}
