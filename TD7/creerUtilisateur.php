<?php

use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;

$login = $_POST["login"];
$nom = $_POST["nom"];
$prenom = $_POST["prenom"];

$newUser = new Utilisateur( $login, $nom, $prenom );

UtilisateurRepository::ajouter($newUser);

