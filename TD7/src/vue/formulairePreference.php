<?php

$utilisateurEnregistre = false;
$trajetEnregistre = false;

if (isset($_COOKIE['preferenceControleur'])){
    $utilisateurEnregistre = true;

    if (json_decode($_COOKIE['preferenceControleur']) == 'utilisateur'){
        $utilisateurEnregistre = true;
    }
    if (json_decode($_COOKIE['preferenceControleur']) == 'trajet'){
        $trajetEnregistre = true;
    }

}

?>
<div>
<h1>Choisir le Contrôleur par Défaut</h1>

<form action="controleurFrontal.php" method="get">
    <fieldset>
        <legend>Préférence du Contrôleur</legend>
        <p>
            <input type="radio" id="utilisateurId" name="controleur_defaut" value="utilisateur" <?php if($utilisateurEnregistre) echo 'checked'; ?> required>
            <label for="utilisateurId">Utilisateur</label>
        </p>
        <p>

            <input type="radio" id="trajetId" name="controleur_defaut" value="trajet" <?php if($trajetEnregistre) echo 'checked'; ?> required>
            <label for="trajetId">Trajet</label>
        </p>
    </fieldset>

    <p>
        <button type="submit">Enregistrer</button>
        <input type='hidden' name='action' value='enregistrerPreference'>
        <input type='hidden' name='controleur' value='generique'>

    </p>
</form>
</div>