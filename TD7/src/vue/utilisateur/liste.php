<?php
/** @var Utilisateur[] $utilisateurs **/

use App\Covoiturage\Modele\DataObject\Utilisateur;

foreach ($utilisateurs as $utilisateur){
    $loginUrl = rawurlencode($utilisateur->getLogin());
    $loginHTML = htmlspecialchars($utilisateur->getLogin());

    echo '<p> Utilisateur de login
        <a href="controleurFrontal.php?action=afficherDetails&login='.$loginUrl.'">'.$loginHTML.'</a></p>';

    echo '<p>
            <a href="controleurFrontal.php?action=supprimer&login='.$loginUrl.'">'."Supprimer".'</a>
            <a href="controleurFrontal.php?action=afficherFormulaireMiseAJour&login='.$loginUrl.'">'."Modifier".'</a>
          </p>';
}

echo '<a href="controleurFrontal.php?action=afficherFormulaireCreation">Créer un utilisateur</a>';

