<?php
use App\Covoiturage\Modele\DataObject\Trajet;

/** @var Trajet $trajet */

$id = $trajet->getId();
$depart = $trajet->getDepart();
$arrivee = $trajet->getArrivee();
$date = $trajet->getDate();
$prix = $trajet->getPrix();
$conducteurlogin = $trajet->getConducteur()->getLogin();

?>

<div>
    <form method="get" action="controleurFrontal.php">
        <fieldset>
            <legend>Mon formulaire :</legend>
            <p>
                <label for="depart_id">Depart</label> :
                <input type="text" value=<?= htmlspecialchars($depart) ?> placeholder="Montpellier" name="depart" id="depart_id" required >
            </p>
            <p>
                <label for="arrivee_id">Arrivée</label> :
                <input type="text" value=<?= htmlspecialchars($arrivee) ?>  placeholder="Sète" name="arrivee" id="arrivee_id" required="">
            </p>
            <p>
                <label for="date_id">Date</label> :
                <input type="date" value=<?= htmlspecialchars($date->format('Y-m-d')) ?> placeholder="JJ/MM/AAAA" name="date" id="date_id" required="">
            </p>
            <p>
                <label for="prix_id">Prix</label> :
                <input type="int" value=<?= htmlspecialchars($prix) ?> placeholder="20" name="prix" id="prix_id" required="">
            </p>
            <p>
                <label for="conducteurLogin_id">Login du conducteur</label> :
                <input type="text" value=<?= htmlspecialchars($conducteurlogin) ?> placeholder="leblancj" name="conducteurLogin" id="conducteurLogin_id" required="">
            </p>
            <p>
                <label for="nonFumeur_id">Non Fumeur ?</label> :
                <input type="checkbox" <?php if($trajet->isNonFumeur()){ echo 'value=checked';}?> placeholder="leblancj" name="nonFumeur" id="nonFumeur_id">
            </p>
            <p>
                <input type="submit" value="Envoyer">
                <input type="hidden" name="controleur" value='trajet'>
                <input type="hidden" name="id" value=<?= $id ?>>
                <input type="hidden" name="action" value='mettreAJour'>
            </p>
        </fieldset>
    </form>
</div>
