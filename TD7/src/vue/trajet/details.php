<?php

/** @var Trajet $trajet **/

use App\Covoiturage\Modele\DataObject\Trajet;

$id = htmlspecialchars($trajet->getId());

echo "<p> Trajet d'id : ".$id."</p>";

$nonFumeur = $trajet->isNonFumeur() ? " non fumeur" : " ";

echo "<p>
        Le trajet {$nonFumeur} du {$trajet->getDate()->format("d/m/Y")} 
        partira de {$trajet->getDepart()} pour aller à {$trajet->getArrivee()}
        (conducteur: {$trajet->getConducteur()->getPrenom()} {$trajet->getConducteur()->getNom()})
      </p>";
