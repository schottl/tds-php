<?php

namespace App\Covoiturage\Lib;

use App\Covoiturage\Modele\HTTP\Cookie;
use App\Covoiturage\Modele\HTTP\Session;

class PreferenceControleur
{
    private static string $clePreference = "preferenceControleur";

    public static function enregistrer(string $preference) : void
    {
        Cookie::enregistrer(PreferenceControleur::$clePreference, $preference);
    }

    public static function lire() : string
    {
        if(self::existe()){
            return Cookie::lire(PreferenceControleur::$clePreference);
        }
        return "utilisateur";
    }

    public static function existe() : bool
    {
        if (Cookie::contient(PreferenceControleur::$clePreference)) {
            return true;
        }
        return false;
    }

    public static function supprimer() : void
    {
        if (self::existe()){
            Cookie::supprimer(PreferenceControleur::$clePreference);
        }
    }
}

