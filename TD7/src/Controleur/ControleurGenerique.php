<?php

namespace App\Covoiturage\Controleur;

use App\Covoiturage\Lib\PreferenceControleur;

class ControleurGenerique
{
    protected static function afficherVue(
        string $cheminVue,
        array $parametres = []) : void
    {
        extract($parametres);
        require __DIR__."/../vue/$cheminVue";

    }

    public static function afficherFormulairePreference()
    {
        self::afficherVue(
            'vueGenerale.php',
            [
                "titre" => "Formulaire préférence",
                "cheminCorpsVue" => "formulairePreference.php",
            ]
        );
    }

    public static function enregistrerPreference()
    {
        if (isset($_GET['controleur_defaut'])) {
            $preference = $_GET['controleur_defaut'];
            PreferenceControleur::enregistrer($preference);
        }
        self::afficherVue(
            'vueGenerale.php',
            [
                "titre" => "Préférence enregistrée",
                "cheminCorpsVue" => "preferenceEnregistree.php",
            ]
        );
    }

}