<?php

namespace App\Covoiturage\Controleur;

use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\HTTP\Cookie;
use App\Covoiturage\Modele\HTTP\Session;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;

class ControleurUtilisateur extends ControleurGenerique
{

    public static function afficherListe() : void
    {
        $utilisateurs = (New UtilisateurRepository)->recuperer();
        self::afficherVue(
            'vueGenerale.php',
            [
                'utilisateurs' => $utilisateurs,
                "titre" => "Liste des utilisateurs",
                "cheminCorpsVue" => "utilisateur/liste.php"
            ],
        );
    }

    public static function afficherDetails() : void
    {
        if(!isset($_GET["login"])) {
            self::afficherErreur("login incorrect ou n'existe pas");
            return;
        }

        $login = $_GET["login"];
        $utilisateur = (New UtilisateurRepository)->recupererParClePrimaire($login);

        if ($utilisateur == null) {
            self::afficherErreur("l'utilisateur n'existe pas");
        }
        else {
            self::afficherVue(
                'vueGenerale.php',
                [
                    'utilisateur' => $utilisateur,
                    "titre" => "Details utilisateur",
                    "cheminCorpsVue" => "utilisateur/details.php"
                ]
            );
        }
    }

    public static function afficherFormulaireCreation() : void
    {
        self::afficherVue(
            'vueGenerale.php',
            [
                "titre" => "Formulaire de creation",
                "cheminCorpsVue" => "utilisateur/formulaireCreation.php"
            ]
        );
    }

    public static function creerDepuisFormulaire(): void
    {
        if (!isset($_GET["login"]) || !isset($_GET["nom"]) || !isset($_GET["prenom"])) {
            self::afficherErreur("login ou nom ou prenom incorrect");
            return;
        }
        $login = $_GET["login"];
        $nom = $_GET["nom"];
        $prenom = $_GET["prenom"];

        $utilisateur = new Utilisateur($login, $nom, $prenom);
        (New UtilisateurRepository)->ajouter($utilisateur);

        self::afficherVue(
            'vueGenerale.php',
            [
                "titre" => "Utilisateur Cree",
                "cheminCorpsVue" => "utilisateur/utilisateurCree.php",
                "utilisateurs" => (New UtilisateurRepository)->recuperer()
            ]
        );
    }

    public static function afficherErreur(string $messageErreur = ""){
        self::afficherVue(
            'vueGenerale.php',
            [
                "titre" => "Erreur",
                "cheminCorpsVue" => "utilisateur/erreur.php"
                ,"messageErreur" => $messageErreur
            ]
        );
    }

    public static function supprimer()
    {
        if (!isset($_GET["login"]))
        {
            self::afficherErreur("Le login doit etre entré en URL");
        }

        else
        {
            $login = $_GET["login"];
            (New UtilisateurRepository)->supprimer($login);
            self::afficherVue(
                'vueGenerale.php',
                [
                    "titre" => "Utilisateur supprimé",
                    "cheminCorpsVue" => "utilisateur/utilisateurSupprime.php",
                    "login" => $login,
                    "utilisateurs" => (New UtilisateurRepository)->recuperer()
                ]
            );
        }
    }

    public static function afficherFormulaireMiseAJour()
    {
        if (!isset($_GET["login"])){
            self::afficherErreur("login pas spécifié dans l'URL");
        }
        else
        {
            $login = $_GET["login"];
            $utilisateur = (New UtilisateurRepository)->recupererParClePrimaire($login);

            self::afficherVue(
                'vueGenerale.php',
                [
                    "titre" => "Formulaire mise à jour",
                    "cheminCorpsVue" => "utilisateur/formulaireMiseAJour.php",
                    "utilisateur" => $utilisateur
                ]
            );
        }
    }

    public static function mettreAJour()
    {
        if (!isset($_GET["login"]) || !isset($_GET["nom"]) || !isset($_GET["prenom"]))
        {
            self::afficherErreur("login ou nom ou prenom incorrect");
        }
        else
        {
            $login = $_GET["login"];
            $nom = $_GET["nom"];
            $prenom = $_GET["prenom"];

            $utilisateur = new Utilisateur($login, $nom, $prenom);

            (New UtilisateurRepository)->mettreAJour($utilisateur);

            self::afficherVue(
                'vueGenerale.php',
                [
                    "titre" => "Mise à jour",
                    "cheminCorpsVue" => "utilisateur/utilisateurMisAJour.php",
                    "login" => $login,
                    "utilisateurs" => (New UtilisateurRepository)->recuperer()
                ]
            );
        }
    }




}
