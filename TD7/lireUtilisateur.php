<?php

use App\Covoiturage\Modele\DataObject\Utilisateur;

$utilisateurs = Utilisateur::recupererUtilisateurs();

foreach ($utilisateurs as $utilisateur) {
    echo $utilisateur . "\n";
}