<?php

require_once 'Trajet.php';

$trajets = Trajet::getTrajets();

foreach ($trajets as $trajet) {
    echo 'Trajet : '.$trajet->getId()."\n";
    echo "Passagers :\n";
    $passagers = $trajet->getPassagers();
    foreach ($passagers as $passager) {
        echo 'Passager : '.$passager->getLogin()."\n";
    }

}