<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title></title>
</head>
<body>
<div>
    <form method="get" action="supprimerUtilisateur.php">
        <fieldset>
            <legend>Supprime un utilisateur par son login :</legend>
            <p>
                <label for="login_id">Login</label> :
                <input type="text" placeholder="Ex : leblancj" name="login" id="login_id" required/>

                <label for="trajet">id trajet</label> :
                <input type="text" placeholder="Ex : 1" name="id" id="trajet_id" required/>

            </p>
            <p>
                <input type="submit" value="Envoyer" />
            </p>
        </fieldset>
    </form>
</div>
<?php

require_once 'ConnexionBaseDeDonnees.php';
require_once 'ModeleUtilisateur.php';
require_once 'Trajet.php';

$id = $_GET["id"];
$passagerLogin = $_GET["login"];

try {
    $trajet = Trajet::recupererTrajetParId($id);
    $trajet->supprimerPassager($passagerLogin);

} catch (Exception $e) {
}

?>
</body>
</html>
