<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title> Mon premier php </title>
    </head>
   
    <body>

        <?php
            $yaniss = [
                'nom' => "Belhaj",
                'prenom' => "Yaniss",
                'login' => "belhajy"
            ];

            $liam = [
                'nom' => "Schott",
                'prenom' => "Liam",
                'login' => "schottl"
            ];

            $users = [
                    '1' => $yaniss,
                    '2' => $liam
            ];



            if(empty($users)){
                echo "Il n'y a aucun utilisateur";
            }
            else {
                echo "Liste des utilisateurs ";

                foreach ($users as $cle => $valeur) {
                    echo "<ul>";

                    foreach ($valeur as $info => $val) {
                        echo '<li>' . $info . ' : ' . $val . '</li>';
                    }
                    echo "</ul>";

                }
            }
        ?>
    </body>
</html> 