<?php
class Utilisateur {

    private String $login;
    private String $nom;
    private String $prenom;

    // un getter
    public function getNom() {
        return $this->nom;
    }

    // un setter
    public function setNom(String $nom) {
        $this->nom = $nom;
    }

    // un constructeur
    public function __construct(
        String $login,
        String $nom,
        String $prenom,
   ) {
        $this->login = $login;
        $this->nom = $nom;
        $this->prenom = $prenom;
    }

    /**
     * @return mixed
     */
    public function getLogin() : String
    {
        return $this->login;
    }

    /**
     * @param mixed $login
     */
    public function setLogin(String $login)
    {
        $this->login = $login;
    }

    /**
     * @return mixed
     */
    public function getPrenom() : String
    {
        return $this->prenom;
    }

    /**
     * @param mixed $prenom
     */
    public function setPrenom(String $prenom)
    {
        $this->prenom = $prenom;
    }



    // Pour pouvoir convertir un objet en chaîne de caractères
    public function __toString() : String  {
       return "Utilisateur : login : ".$this->login. " nom : ".$this->nom." prenom : ".$this->prenom;
    }


}
?>