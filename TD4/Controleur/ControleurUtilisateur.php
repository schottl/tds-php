<?php
require_once ('../Modele/ModeleUtilisateur.php'); // chargement du modèle

class ControleurUtilisateur
{

    private static function afficherVue(
        string $cheminVue,
        array $parametres = []) : void
    {
        extract($parametres);
        require "../vue/$cheminVue"; // Charge la vue
    }

    public static function afficherListe() : void
    {
        $utilisateurs = ModeleUtilisateur::recupererUtilisateurs();
        self::afficherVue(
            'utilisateur/liste.php',
            ['utilisateurs' => $utilisateurs]
        );
    }

    public static function afficherDetails() : void
    {
        if(!isset($_GET["login"])) {
            self::afficherVue(
                'utilisateur/erreur.php'
            );
            return;
        }

        $login = $_GET["login"];
        $utilisateur = ModeleUtilisateur::getUtilisateurParLogin($login);

        if ($utilisateur == null) {
            self::afficherVue(
                'utilisateur/erreur.php'
            );
        }
        else {
            self::afficherVue(
                'utilisateur/details.php',
                ['utilisateur' => $utilisateur]
            );  //"redirige" vers la vue
        }
    }

    public static function afficherFormulaireCreation() : void
    {
        self::afficherVue(
            'utilisateur/formulaireCreation.php',
        );
    }

    public static function creerDepuisFormulaire(): void
    {
        if (!isset($_GET["login"]) || !isset($_GET["nom"]) || !isset($_GET["prenom"])) {
            self::afficherVue(
                'utilisateur/erreur.php'
            );
            return;
        }

        $login = $_GET["login"];
        $nom = $_GET["nom"];
        $prenom = $_GET["prenom"];

        $utilisateur = new ModeleUtilisateur($login, $nom, $prenom);
        $utilisateur->ajouter();

        self::afficherListe();

    }



}
