<?php
/** @var ModeleUtilisateur[] $utilisateurs **/
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Liste des utilisateurs</title>
</head>
<body>
<?php
foreach ($utilisateurs as $utilisateur){
  echo '<p> Utilisateur de login <a href="../Controleur/routeur.php?action=afficherDetails&login='.urlencode($utilisateur->getLogin()).'">'.htmlspecialchars($utilisateur->getLogin()) . '</a></p>';
}

echo '<a href="../Controleur/routeur.php?action=afficherFormulaireCreation">Créer un utilisateur</a>';
?>
</body>
</html>
